CREATE TABLE trophies (
    _id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(_id),
    name VARCHAR(255),
    score BIGINT,
    date DATE
)

alter table trophies add column bosslevel int;
update trophies set bosslevel = 0;