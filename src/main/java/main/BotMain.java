package main;

import org.pircbotx.Configuration;
import org.pircbotx.IdentServer;
import org.pircbotx.PircBotX;
import util.DBUtil;

import java.io.InputStream;
import java.util.Properties;

public class BotMain {

    private static final String configFile = "pirkko.properties";

    public static void main(String[] args) throws Exception {

        InputStream input = BotMain.class.getClassLoader().getResourceAsStream(configFile);
        Properties props = new Properties();
        props.load(input);
        input.close();

        DBUtil.initDB(props.getProperty("database.host", "localhost")
                ,Integer.parseInt(props.getProperty("database.port", "27017"))
                ,props.getProperty("database.name", "pirkko")
                ,props.getProperty("database.user","pirkko")
                ,props.getProperty("database.password","pirkko")
        );

        Pirkko pirkko = new Pirkko(props);

        Configuration.Builder configBuilder = new Configuration.Builder();
        configBuilder.setName(props.getProperty("nick"))
                .setServerHostname(props.getProperty("server"))
                .setLogin(props.getProperty("login"))
                .setAutoNickChange(true)
                .setAutoReconnect(true)
                .addListener(pirkko)
                .setAutoReconnect(true);

        if(props.getProperty("use.ident.server","false").equals("true")) {
            try {
                IdentServer.startServer();
            } catch (Exception e) {
                System.out.println("Failed to start IdentServer: " + e);
            }
            configBuilder.setIdentServerEnabled(true);
        }
        for(String channel : props.getProperty("channels","").split(",")) {
            configBuilder.addAutoJoinChannel(channel);
        }

        Configuration configuration = configBuilder.buildConfiguration();

        PircBotX bot = new PircBotX(configuration);
        bot.startBot();
    }
}