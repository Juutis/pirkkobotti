package main;

import model.RPGUser;
import org.pircbotx.Channel;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NickChangeEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;
import util.DBUtil;
import java.util.Properties;

/**
 * Created by KM on 6.8.2015.
 */
public class Pirkko extends ListenerAdapter {

    private UserManager userManager;
    private ServiceManager services;

    public Pirkko(Properties props) {
        super();
        userManager = new UserManager(DBUtil.getUsers());
        services = new ServiceManager(userManager);
    }

    @Override
    public void onNickChange(NickChangeEvent event) {
        userManager.renameUser(event.getOldNick(), event.getNewNick());
    }

    @Override
    public void onGenericMessage(GenericMessageEvent event) {

        if(event instanceof MessageEvent) {
            MessageEvent messageEvent = (MessageEvent)event;

            Channel channel = messageEvent.getChannel();
            RPGUser sender = userManager.getUser(event.getUser().getNick());
            String message = messageEvent.getMessage();

            services.handleEvent(channel, sender, messageEvent);

            String response = null;

            if (((message.indexOf("Pirkko") != -1) || (message.indexOf("pirkko") != -1)) && (message.indexOf("11.") != -1)) {
                response = "PENIS PENIS PENIS MASTURBAAAAAATIO";
            }
            if (((message.indexOf("Pirkko") != -1) || (message.indexOf("pirkko") != -1)) && (message.indexOf("loska") != -1)) {
                response = "MIÄ HEITÄN LOSKAA ON MÄRKÄ JA KYLMÄÄ T:PIRKO";
            }
            if (message.equals("(monkey)")) {
                channel.send().message("    .=\"=.");
                channel.send().message("  _/.-.-.\\_ ");
                channel.send().message(" ( ( o o ) )");
                channel.send().message("  |/  \"  \\| ");
                channel.send().message("   \\'---'/ ");
                channel.send().message("    `\"\"\"`");
            }

            if(response != null && !response.isEmpty()) {
                channel.send().message(response);
            }
        }
        if(event instanceof PrivateMessageEvent) {
            PrivateMessageEvent privEvent = (PrivateMessageEvent)event;
            RPGUser sender = userManager.getUser(event.getUser().getNick());
            services.handleEvent(sender,privEvent);
        }
    }
}
