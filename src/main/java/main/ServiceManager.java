package main;

import model.RPGUser;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import rpg.RPGHandler;
import rpg.blackjack.BlackJackMain;
import rpg.dungeon.DungeonHandler;
import rpg.revolveri.RevolveriInput;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by KM on 10.8.2015.
 */
public class ServiceManager {
    private HashMap<String, List<Service>> channelServices;
    private HashMap<String, List<Service>> userServices;
    private List<Service> globalServices;
    private UserManager userManager;

    public ServiceManager(UserManager um) {
        userManager = um;

        channelServices = new HashMap<String, List<Service>>();
        userServices = new HashMap<String, List<Service>>();
        globalServices = new LinkedList<Service>();
        globalServices.add(new RPGHandler(userManager));
    }

    public void handleEvent(Channel channel, RPGUser sender, MessageEvent event) {
        for(Service service : getServices(channel)) {
            String response = service.handleMessage(sender, event);
            if(response != null && !response.isEmpty()) {
                channel.send().message(response);
            }
        }

        for(Service service : getUserServices(sender)) {
            String response = service.handleMessage(sender, event);
            if(response != null && !response.isEmpty()) {
                channel.send().message(response);
            }
        }
    }

    public void handleEvent(RPGUser sender, PrivateMessageEvent event) {
        for(Service service : globalServices) {
            String response = service.handleMessage(sender, event);
            if(response != null && !response.isEmpty()) {
                event.getUser().send().message(response);
            }
        }
        for(Service service : getUserServices(sender)) {
            String response = service.handleMessage(sender, event);
            if(response != null && !response.isEmpty()) {
                event.getUser().send().message(response);
            }
        }
    }

    public List<Service> getServices(Channel channel) {
        List<Service> result = new LinkedList<Service>(globalServices);
        result.addAll(getChannelServices(channel));
        return result;
    }

    public List<Service> getChannelServices(Channel channel) {
        String chName = channel.getName();
        List<Service> result;
        if(!channelServices.containsKey(chName)) {
            result = new LinkedList<Service>();
            result.add(new BlackJackMain(channel));
            result.add(new RevolveriInput(channel));
            channelServices.put(chName, result);
        } else {
            result = channelServices.get(chName);
        }
        return result;
    }

    public List<Service> getUserServices(RPGUser user) {
        String userName = user.getNick();
        List<Service> result;
        if(!userServices.containsKey(userName)) {
            result = new LinkedList<Service>();
            result.add(new DungeonHandler());
            userServices.put(userName, result);
        } else {
            result = userServices.get(userName);
        }
        return result;
    }

}
