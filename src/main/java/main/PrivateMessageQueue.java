package main;

import org.pircbotx.User;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by KM on 12.8.2015.
 */
public class PrivateMessageQueue extends Thread {

    private User user;
    private boolean kill = false, dead = false;
    private LinkedList<String> queue;
    private CallBack callback;
    private long delay;

    public PrivateMessageQueue(User user, CallBack callback, long callbackDelay) {
        this.user = user;
        this.queue = new LinkedList<String>();
        this.callback = callback;
        this.delay = callbackDelay;
        start();
    }

    @Override
    public void run() {
        while(!kill || !queue.isEmpty()) {
            try {
                sleep(1500);
            } catch(Exception e) {
            }
            if(!queue.isEmpty()) {
                user.send().message(queue.poll());
            }
        }
        dead = true;
        try {
            sleep(delay);
        } catch(Exception e) {}
        callback.callback();
    }

    public void addMessage(String text) {
        queue.offer(text);
    }

    public void kill() {
        kill = true;
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isReady() {
        return queue.isEmpty();
    }
}
