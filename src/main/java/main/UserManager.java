package main;

import model.RPGUser;
import util.DBUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by KM on 6.8.2015.
 */
public class UserManager {

    HashMap<String, RPGUser> users;

    public UserManager(Collection<RPGUser> users) {
        this.users = new HashMap<String, RPGUser>();
        for(RPGUser user : users) {
            this.users.put(user.getNick(), user);
        }
    }

    public RPGUser getUser(String nick) {
        RPGUser user;
        if(!users.containsKey(nick)) {
            user = new RPGUser(nick);
            DBUtil.saveUser(user);
            users.put(nick, user);
        } else {
            user = users.get(nick);
        }
        return user;
    }

    public void renameUser(String oldNick, String newNick) {
        RPGUser user = getUser(oldNick);

        user.setNick(newNick);
        DBUtil.saveUser(user);

        users.remove(oldNick);
        users.put(newNick, user);
    }

    public Collection<RPGUser> getUsers() {
        ArrayList<RPGUser> result = new ArrayList<RPGUser>(users.size());
        for(String key : users.keySet()) {
            result.add(users.get(key));
        }
        return result;
    }

}
