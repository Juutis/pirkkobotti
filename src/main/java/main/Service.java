package main;

import model.RPGUser;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

/**
 * Created by KM on 10.8.2015.
 */
public interface Service {

    public String handleMessage(RPGUser sender, MessageEvent event);
    public String handleMessage(RPGUser sender, PrivateMessageEvent event);
}
