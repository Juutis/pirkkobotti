package rpg.dungeon;

import model.Creature;
import model.RPGUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rpg.dungeon.boss.Aborttikenguru;
import rpg.dungeon.boss.Pirkko;
import rpg.dungeon.boss.Skeletor;
import util.CommonUtil;

import java.io.InputStream;
import java.util.*;

/**
 * Created by KM on 11.8.2015.
 */
public class CreatureGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(CreatureGenerator.class);

    private static final String CREATUREFILE = "creatures.properties";
    private static String[] CREATURES_WEAK, CREATURES_NORMAL, CREATURES_STRONG,
                            MODIFIERS_WEAK, MODIFIERS_NORMAL, MODIFIERS_STRONG,
                            TYPES_STRONG, TYPES_WEAK,
                            SPECIFIERS_STRONG,
                            POSTFIXES_STRONG,
                            POSTFIX_MODIFIERS_STRONG;

    private static final long   XP_CREATURE_WEAK = 25,
                                XP_CREATURE_NORMAL = 500,
                                XP_CREATURE_STRONG = 10000;

    private static final double MIN_CHANCE = 0.05,
                                MAX_CHANCE = 0.9,
                                FACTOR_MODIFIER_WEAK = 0.5,
                                FACTOR_MODIFIER_NORMAL = 5,
                                FACTOR_MODIFIER_STRONG = 20,
                                FACTOR_TYPE_STRONG = 30,
                                FACTOR_TYPE_WEAK = 0.5,
                                FACTOR_SPECIFIER_STRONG = 50,
                                FACTOR_POSTFIX_STRONG = 100,
                                FACTOR_POSTFIX_MODIFIER_STRONG = 100,
                                BOSSCHANCE = 0.10;

    private static final int MAX_MODIFIERS = 5;

    private static CreatureGenerator singleton;

    private Random random;

    private Creature[] bosses = { new Skeletor(), new Pirkko(), new Aborttikenguru()};

    public static CreatureGenerator getSingleton() {
        if(singleton == null) {
            singleton = new CreatureGenerator();
        }
        return singleton;
    }

    private CreatureGenerator() {

        InputStream input = getClass().getClassLoader().getResourceAsStream(CREATUREFILE);
        Properties props = new Properties();
        try {
            props.load(input);
        } catch (Exception e) {
            LOG.error("Couln't read file " + CREATUREFILE, e);
        }
        CREATURES_WEAK = props.getProperty("weak.creatures","").split(",");
        CREATURES_NORMAL = props.getProperty("normal.creatures","").split(",");
        CREATURES_STRONG = props.getProperty("strong.creatures","").split(",");
        MODIFIERS_WEAK = props.getProperty("weak.modifiers","").split(",");
        MODIFIERS_NORMAL = props.getProperty("normal.modifiers","").split(",");
        MODIFIERS_STRONG = props.getProperty("strong.modifiers","").split(",");
        TYPES_STRONG = props.getProperty("strong.types","").split(",");
        TYPES_WEAK = props.getProperty("weak.types","").split(",");
        SPECIFIERS_STRONG = props.getProperty("strong.specifiers","").split(",");
        POSTFIXES_STRONG = props.getProperty("strong.postfixes","").split(",");
        POSTFIX_MODIFIERS_STRONG = props.getProperty("strong.postfix.modifiers","").split(",");

        random = new Random();
    }

    public Creature getBoss(Trophies trophies) {
        for(int i = 0; i < bosses.length; i++) {
            Creature boss = bosses[i];
            if(!trophies.containsBoss(boss)) {
                return boss;
            }
        }
        return null;
    }

    public Creature getCreature(int level, RPGUser user, int depth, boolean hc) {

        double bossChane = BOSSCHANCE * (hc ? 3 : 1) * depth;

        if(user != null && level == 10 && random.nextDouble() < bossChane) {
            Creature boss = getBoss(user.getTrophies());
            if(boss != null) return boss;
        }

        String creatureName = "", creatureNameShort = "", creature = "", type = "", specifier = "", postfix1 = "", postfix2 = "", postfixModifier = "";
        long creatureXp;
        List<String> modifiers = new ArrayList<String>();

        double weakChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.15 * (level) * (level) + 1.0));
        double strongChance = Math.min(MAX_CHANCE/(1.0 - weakChance), Math.max(MIN_CHANCE/(1.0 - weakChance),(-0.05 * (level - 10.0) * (level - 10.0) + 1.0)/(1.0 - weakChance)));

        if(random.nextDouble() < weakChance) {
            creature = pickRandomString(CREATURES_WEAK);
            creatureXp = XP_CREATURE_WEAK;
        } else if (random.nextDouble() < strongChance) {
            creature = pickRandomString(CREATURES_STRONG);
            creatureXp = XP_CREATURE_STRONG;
        } else {
            creature = pickRandomString(CREATURES_NORMAL);
            creatureXp = XP_CREATURE_NORMAL;
        }

        double weakModifierChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.15 * (level - 1) * (level - 1) + 1.0));
        double normalModifierChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.05 * (level - 5) * (level - 5) + 0.8));
        double strongModifierChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE,-0.05 * (level - 10) * (level - 10) + 1));

        double chance;
        String modifier, postfix;

        chance = strongModifierChance;
        while(modifiers.size() < MAX_MODIFIERS && random.nextDouble() < chance) {
            chance *=  strongModifierChance;
            do {
                modifier = pickRandomString(MODIFIERS_STRONG);
            } while(modifiers.contains(modifier) || (modifier.indexOf("headed") > -1 && CommonUtil.containsSubstring(modifiers, "headed")));
            modifiers.add(modifier);
            creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_MODIFIER_STRONG);
        }
        chance = normalModifierChance;
        while(modifiers.size() < MAX_MODIFIERS && random.nextDouble() < chance) {
            chance *=  normalModifierChance;
            do {
                modifier = pickRandomString(MODIFIERS_NORMAL);
            } while(modifiers.contains(modifier));
            modifiers.add(modifier);
            creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_MODIFIER_NORMAL);
        }
        chance = weakModifierChance;
        while(modifiers.size() < MAX_MODIFIERS && random.nextDouble() < chance) {
            chance *=  weakModifierChance;
            do {
                modifier = pickRandomString(MODIFIERS_WEAK);
            } while(modifiers.contains(modifier));
            modifiers.add(modifier);
            creatureXp *= FACTOR_MODIFIER_WEAK;
        }

        double weakTypeChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.001 * level*level + 0.1));
        double strongTypeChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.04 * (level - 10) * (level - 10) + 1.0));
        if(random.nextDouble() < weakTypeChance) {
            type = pickRandomString(TYPES_WEAK);
            creatureXp *= FACTOR_TYPE_WEAK;
        } else if(random.nextDouble() < strongTypeChance) {
            type = pickRandomString(TYPES_STRONG);
            creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_TYPE_STRONG);
        }

        double specifierChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.08 * (level - 10) * (level - 10) + 1.0));
        if(random.nextDouble() < specifierChance) {
            specifier = pickRandomString(SPECIFIERS_STRONG);
            creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_SPECIFIER_STRONG);
        }

        double postfixChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.3 * (level - 10) * (level - 10) + 1.0));
        double postfixModifierChance = Math.min(MAX_CHANCE, Math.max(MIN_CHANCE, -0.5 * (level - 10) * (level - 10) + 1.0));
        if(random.nextDouble() < postfixChance) {
            postfix1 = pickRandomString(POSTFIXES_STRONG);
            creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_POSTFIX_STRONG);

            if(random.nextDouble() < postfixModifierChance) {
                postfixModifier = pickRandomString(POSTFIX_MODIFIERS_STRONG);
                creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_POSTFIX_MODIFIER_STRONG);
            }
            if(random.nextDouble() < postfixChance) {
                do {
                    postfix = pickRandomString(POSTFIXES_STRONG);
                } while(postfix.equals(postfix1));
                postfix2 = postfix;
                creatureXp = CommonUtil.multiply(creatureXp, (long)FACTOR_POSTFIX_STRONG);
            }
        }

        String[] modsArray = modifiers.toArray(new String[modifiers.size()]);
        CommonUtil.shuffle(modsArray);
        for(String s : modsArray) {
            if(CommonUtil.hasContent(s)) {
                creatureName += s + " ";
            }
        }
        if(CommonUtil.hasContent(type)) {
            creatureName += CommonUtil.capitalize(type) + " ";
            creatureNameShort += CommonUtil.capitalize(type) + " ";
        }
        if(CommonUtil.hasContent(creature)) {
            creatureName += CommonUtil.capitalize(creature);
            creatureNameShort += CommonUtil.capitalize(creature);
        }
        if(CommonUtil.hasContent(specifier)) {
            creatureName += " " + CommonUtil.capitalize(specifier);
            creatureNameShort += " " + CommonUtil.capitalize(specifier);
        }
        if(CommonUtil.hasContent(postfix1)) {
            creatureName += " of ";
            if(CommonUtil.hasContent(postfixModifier)) {
                creatureName += CommonUtil.capitalize(postfixModifier) + " ";
            }
            creatureName += CommonUtil.capitalize(postfix1);
        }
        if(CommonUtil.hasContent(postfix2)) {
            creatureName += " and " + CommonUtil.capitalize(postfix2);
        }

        creatureName = creatureName.toUpperCase();
        creatureNameShort = creatureNameShort.toUpperCase();
        double factor = 0.9 + random.nextDouble()/5.0;
        if(creatureXp > (double)Long.MAX_VALUE / factor) {
            creatureXp = Long.MAX_VALUE;
        } else {
            creatureXp *= factor;
        }
        if(creatureXp < 1) creatureXp = 1;

        return new Creature(creatureName,creatureNameShort,creatureXp,0);
    }

    private String pickRandomString(String[] array) {
        return array[random.nextInt(array.length)];
    }


    public static void main(String[] args) {

        CreatureGenerator cg = new CreatureGenerator();

        double[] xps = new double[10];
        long[][] xps2 = new long[10][100000];
        for(int i = 0; i < xps.length; i++) {
            xps[i] = 0;
        }

        for(int j = 0; j < 100000; j++) {
            for (int i = 1; i <= 10; i++) {
                Creature c = cg.getCreature(i, null, 0, false);
                xps[i-1] += (double)c.getXpBounty() / 100000.0;
                xps2[i-1][j] = c.getXpBounty();
            }
        }

        for(int i = 0; i < 10; i++) {
            Arrays.sort(xps2[i]);
        }

        for(int i = 0; i < xps.length; i++) {
            System.out.println((i+1) + " mean = " + xps[i]);
            System.out.println("median = " + xps2[i][xps2[i].length / 2]);


        }
    }
}
