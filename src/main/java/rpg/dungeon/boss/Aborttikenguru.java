package rpg.dungeon.boss;

import model.Creature;

/**
 * Created by KM on 16.8.2015.
 */
public class Aborttikenguru extends Creature {

    public Aborttikenguru() {
        super("Aborttikenguru", "Aborttikenguru", Long.MAX_VALUE, 3);
    }
}
