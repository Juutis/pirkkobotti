package rpg.dungeon.boss;

import model.Creature;

/**
 * Created by KM on 17.8.2015.
 */
public class Pirkko extends Creature{

    public Pirkko() {
        super("Pirkko", "Pirkko", Long.MAX_VALUE, 2);
    }
}
