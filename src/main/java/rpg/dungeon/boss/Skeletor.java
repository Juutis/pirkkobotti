package rpg.dungeon.boss;

import model.Creature;

/**
 * Created by KM on 17.8.2015.
 */
public class Skeletor extends Creature {

    public Skeletor() {
        super("Skeletor", "Skeletor", Long.MAX_VALUE, 1);
    }
}
