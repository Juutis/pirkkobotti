package rpg.dungeon.boss;

import model.Creature;
import model.RPGCharacter;
import model.Strike;
import rpg.dungeon.Fighter;

/**
 * Created by KM on 17.8.2015.
 */
public class BossFighter extends Fighter{

    public BossFighter(RPGCharacter creature) {
        super(creature);
    }

    @Override
    public Strike strike(Fighter fighter) {

        Strike strike = new Strike(Long.MAX_VALUE, Strike.TYPE.CRIT);
        double hitRoll = random.nextDouble();
        if(hitRoll >= (1.0 - criticalMissChance)) {
            strike.setType(Strike.TYPE.CRITMISS);
        }
        return strike;
    }
}
