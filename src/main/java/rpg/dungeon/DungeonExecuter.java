package rpg.dungeon;

import main.CallBack;
import main.PrivateMessageQueue;
import model.Creature;
import model.RPGUser;
import model.Strike;
import model.Trophy;
import org.pircbotx.Channel;
import org.pircbotx.User;
import rpg.dungeon.boss.BossFighter;
import util.CommonUtil;
import util.UserUtil;

import java.util.Random;

/**
 * Created by KM on 12.8.2015.
 */
public class DungeonExecuter implements CallBack {

    private User target;
    private RPGUser rpgUser;
    private Channel channel;
    private int dungeonLevel;
    private PrivateMessageQueue messageQueue;
    private Dungeon dungeon;
    private boolean finished = false;

    private Fighter player, enemy;
    private Random random;
    private long totalXp = 0;
    private Creature strongestSlain = null, cEnemy;
    private boolean hcmode;

    public DungeonExecuter(int dungeonLevel, boolean hc, User target, RPGUser rpgUser, Channel initChannel) {
        this.target = target;
        this.rpgUser = rpgUser;
        this.channel = initChannel;
        this.dungeonLevel = dungeonLevel;
        this.player = new Fighter(rpgUser);
        this.hcmode = hc;
        messageQueue = new PrivateMessageQueue(this.target, this, 1500);
        random = new Random();
    }

    public void startDungeon() {
        dungeon = new Dungeon(dungeonLevel, rpgUser, hcmode);
        messageQueue.addMessage(dungeon.getStartText());
        if(!hcmode) {
            messageQueue.addMessage("Type !stats to check your stats or !examine to examine your opponent.");
        } else {
            messageQueue.addMessage("!stats and !examine are disabled in HC mode.");
        }
        enemy = new Fighter(dungeon.getEnemy());
        messageQueue.addMessage(dungeon.getFightText());
    }

    public void handleDungeonMessage(String text) {
        if(messageQueue.isReady() && player.getHealth() > 0) {
            if (text.equalsIgnoreCase("yes") || text.equalsIgnoreCase("y")) {
                cEnemy = dungeon.getEnemy();
                messageQueue.addMessage("You engage the " + cEnemy.getShortName() + ".");
                if(cEnemy.getBossLevel() > 0) {
                    enemy = new BossFighter(cEnemy);
                } else {
                    enemy = new Fighter(cEnemy);
                }
                if (fight()) {
                    rpgUser.getTrophies().createThrophyFromCreature(cEnemy);
                    if(strongestSlain == null || strongestSlain.getBossLevel() < cEnemy.getBossLevel() || strongestSlain.getExperience() < dungeon.getEnemy().getExperience()) {
                        strongestSlain = dungeon.getEnemy();
                    }
                    long bounty = CommonUtil.multiply(dungeon.getDepth(), cEnemy.getXpBounty());
                    if(hcmode) {
                        bounty = CommonUtil.multiply(bounty, 10);
                        messageQueue.addMessage("The " + cEnemy.getName() + " was slain. You gain 10x"
                                + dungeon.getDepth() + "x" + cEnemy.getXpBounty() + "=" + bounty + " experience levels.");
                    } else {
                        messageQueue.addMessage("The " + cEnemy.getName() + " was slain. You gain "
                                + dungeon.getDepth() + "x" + cEnemy.getXpBounty() + "=" + bounty + " experience levels.");
                    }
                    totalXp = CommonUtil.add(totalXp,bounty);
                    if(UserUtil.addXp(rpgUser, bounty)) {
                        UserUtil.fireMaxLevelMessage(channel,rpgUser);
                    }
                    if(cEnemy.getBossLevel() >= Trophy.BOSS_LEVELS.length - 1) {
                        UserUtil.fireReallyWonMessage(channel, rpgUser);
                    }
                    player.levelUp();
                    dungeon.goDeeper();
                    messageQueue.addMessage("You venture deeper into the dungeon...");
                    messageQueue.addMessage(dungeon.getFightText());
                } else {
                    messageQueue.addMessage(dungeon.getDefeatedText());
                    UserUtil.setXp(rpgUser,0);
                    messageQueue.kill();
                    finished = true;
                }
            } else if (text.equalsIgnoreCase("no") || text.equalsIgnoreCase("n")) {
                messageQueue.addMessage(dungeon.getLeaveText());
                messageQueue.kill();
                finished = true;
            } else if (text.equalsIgnoreCase("!stats")) {
                if(hcmode) {
                    messageQueue.addMessage("!stats is disabled in HC mode.");
                } else {
                    long hpPercentage = Math.round(100 * ((double) player.getHealth() / (double) player.getMaxHealth()));
                    messageQueue.addMessage("Health: " + player.getHealth() + "/" + player.getMaxHealth() + " (" + hpPercentage + "%), " +
                            "Damage: " + player.getMinDamage() + "-" + player.getMaxDamage() + ", " +
                            "Current dungeon depth: " + dungeon.getDepth() + ", " +
                            "XP gained from this dungeon so far: " + totalXp);
                }
            } else if (text.equalsIgnoreCase("!examine")) {
                if(hcmode) {
                    messageQueue.addMessage("!examine is disabled in HC mode.");
                } else {
                    messageQueue.addMessage(examine());
                }
            }
        }
    }

    private boolean fight() {

        if(random.nextDouble() < 0.5 || cEnemy.getBossLevel() > 0) {
            if(doPlayerTurn()) return true;
        }

        while(true) {
            if(!enemy.isStunned()) {
                if (doEnemyTurn()) return false;
            } else {
                enemy.stunTick();
            }

            if(!player.isStunned()) {
                if(doPlayerTurn()) return true;
            } else {
                player.stunTick();
            }
        }
    }

    private boolean doPlayerTurn() {
        Strike strike = player.strike(enemy);
        boolean dead = false;
        switch(strike.getType()) {
            case HIT:
                messageQueue.addMessage("You HIT the " + dungeon.getEnemy().getShortName() + " for " + strike.getDamage() + " damage.");
                dead = enemy.damage(strike.getDamage());
                break;
            case CRIT:
                messageQueue.addMessage("You CRITICALLY hit the " + dungeon.getEnemy().getShortName() + " for " + strike.getDamage() + " damage!");
                dead = enemy.damage(strike.getDamage());
                break;
            case MISS:
                messageQueue.addMessage("You MISS the " + dungeon.getEnemy().getShortName() + ".");
                break;
            case CRITMISS:
                messageQueue.addMessage("You CRITICALLY MISS the " + dungeon.getEnemy().getShortName() + ".");
                player.stun(1);
                break;
        }
        return dead;
    }

    private boolean doEnemyTurn() {
        Strike strike = enemy.strike(player);
        boolean dead = false;
        switch(strike.getType()) {
            case HIT:
                messageQueue.addMessage("The " + dungeon.getEnemy().getShortName() + " HITs you for " + strike.getDamage() + " damage.");
                dead = player.damage(strike.getDamage());
                break;
            case CRIT:
                messageQueue.addMessage("The " + dungeon.getEnemy().getShortName() + " CRITICALLY HITs you for " + strike.getDamage() + " damage!");
                dead = player.damage(strike.getDamage());
                break;
            case MISS:
                messageQueue.addMessage("The " + dungeon.getEnemy().getShortName() + " MISSes you.");
                break;
            case CRITMISS:
                messageQueue.addMessage("The " + dungeon.getEnemy().getShortName() + " CRITICALLY MISSes you.");
                enemy.stun(1);
                break;
        }
        return dead;
    }

    public boolean isFinished() {
        return finished && messageQueue.isDead();
    }

    public void callback() {
        if(player.getHealth() <= 0) {
            channel.send().message(rpgUser.getNick() + " ei selvinnyt takasin aloittamaltaan seikkailulta.");
        } else if(totalXp == 0) {
            channel.send().message(rpgUser.getNick() + " luikkii häntä koipien välissä seikkailulta takaisin taistelematta kertaakaan.");
        } else if(player.getHealth() == player.getMaxHealth()) {
            channel.send().message(rpgUser.getNick() + " saapuu takaisin seikkailulta ilman naarmun naarmua " + totalXp + " kokemustasoa rikkaampana! " +
                    "Voimallisin saalis: " + strongestSlain.getName() + " (" + strongestSlain.getExperienceString() + ").");
        } else {
            double hp = (double) player.getHealth() / (double) player.getMaxHealth();
             if(hp < 0.2) {
                channel.send().message(rpgUser.getNick() + " saapuu henkihieverissä takaisin seikkailulta " + totalXp + " kokemustasoa rikkaampana! " +
                        "Voimallisin saalis: " + strongestSlain.getName() + " (" + strongestSlain.getExperienceString() + ").");
            } else if(hp < 0.6) {
                channel.send().message(rpgUser.getNick() + " saapuu väsyneenä takaisin seikkailulta " + totalXp + " kokemustasoa rikkaampana! " +
                        "Voimallisin saalis: " + strongestSlain.getName() + " (" + strongestSlain.getExperienceString() + ").");
            } else {
                channel.send().message(rpgUser.getNick() + " saapuu voitokkaana takaisin seikkailulta " + totalXp + " kokemustasoa rikkaampana! " +
                        "Voimallisin saalis: " + strongestSlain.getName() + " (" + strongestSlain.getExperienceString() + ").");
            }
        }
    }

    public String examine() {
        if(dungeon.getEnemy().getBossLevel() > 0) {
            return "???";
        }

        double d = (double)rpgUser.getExperience() / (double)dungeon.getEnemy().getExperience();
        String enemyName = dungeon.getEnemy().getName();
        if(d >= 10.0) {
            return "The " + enemyName + " should be trivial to handle.";
        } else if(d > 2.0) {
            return "The " + enemyName + " should go down easily enough.";
        } else if(d > 1.2) {
            return "The " + enemyName + " might put up a minor fight.";
        } else if(d >= 0.8) {
            return "The " + enemyName + " looks like an evenly matched opponent.";
        } else if(d >= 0.5) {
            return "The " + enemyName + " looks tough to take care of.";
        } else if(d >= 0.1) {
            return "The " + enemyName + " appears to be able to swallow you whole.";
        } else {
            return "You're fucked.";
        }
    }


}
