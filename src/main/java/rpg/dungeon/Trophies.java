package rpg.dungeon;

import model.Creature;
import model.RPGUser;
import model.Trophy;
import rpg.RPGHandler;
import util.DBUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by KM on 14.8.2015.
 */
public class Trophies {

    private List<Trophy> trophyList;
    private List<Trophy> topTrophies;
    private RPGUser user;

    public Trophies(RPGUser user) {
        this.user = user;
        trophyList = new ArrayList<Trophy>();
        topTrophies = new ArrayList<Trophy>(RPGHandler.TOPLIST_LENGTH + 1);
    }

    public void addTrophy(Trophy trophy) {
        trophyList.add(trophy);

        int i = 0;
        for(Trophy t : topTrophies) {
            if(trophy.getExperience() >= t.getExperience()) {
                break;
            }
            i++;
        }
        if(i < RPGHandler.TOPLIST_LENGTH) {
            topTrophies.add(i, trophy);
        }
    }

    public void createThrophyFromCreature(Creature creature) {
        Trophy trophy = new Trophy(creature.getName(), creature.getExperience(), new Date(), user, creature.getBossLevel());
        addTrophy(trophy);
        DBUtil.saveTrophy(trophy);
    }

    public int size() {
        return trophyList.size();
    }

    public Collection<Trophy> getTopTrophies() {
        return topTrophies;
    }

    public boolean containsBoss(Creature c) {
        for (Trophy t : trophyList) {
            if(t.getName().equals(c.getName())) {
                return true;
            }
        }
        return false;
    }

}
