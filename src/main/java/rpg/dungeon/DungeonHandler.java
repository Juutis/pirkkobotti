package rpg.dungeon;

import main.Service;
import model.RPGUser;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

/**
 * Created by KM on 12.8.2015.
 */
public class DungeonHandler implements Service {

    private static final String START_ADVENTURE = "!start adventure";

    private DungeonExecuter dungeonExecuter = null;

    public String handleMessage(RPGUser user, MessageEvent event) {
        if(event.getMessage().startsWith(START_ADVENTURE)) {
            if(user.getExperience() < 1) {
                return user.getNick() + ": Tarvitset ainakin yhden kokemustason seikkaillaksesi.";
            }

            if(dungeonExecuter != null && !dungeonExecuter.isFinished()) {
                return user.getNick() + ": Olet jo seikkailemassa.";
            }

            String[] split = event.getMessage().split(" ");
            try {
                int level = Math.min(10,Math.max(1,Integer.parseInt(split[2])));
                boolean hc = false;
                if(split.length >= 4 && split[3].equals("hc")) {
                    hc = true;
                }
                dungeonExecuter = new DungeonExecuter(level, hc, event.getUser(), user, event.getChannel());
                dungeonExecuter.startDungeon();
                return user.getNick() + " aloittaa seikkailun tason " + level + " tyrmään.";
            } catch(Exception e) {
                return user.getNick() + ": Määritä minkä tason tyrmään haluat seikkailla (1-10).";
            }
        }
        return null;
    }

    public String handleMessage(RPGUser sender, PrivateMessageEvent event) {
        if(dungeonExecuter != null && !dungeonExecuter.isFinished()) {
            dungeonExecuter.handleDungeonMessage(event.getMessage());
        }
        return null;
    }
}
