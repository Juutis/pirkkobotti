package rpg.dungeon;

import model.Strike;
import model.RPGCharacter;
import util.CommonUtil;

import java.util.Random;

/**
 * Created by KM on 13.8.2015.
 */
public class Fighter {

    protected double  criticalHitChance = 0.1,
                    criticalMissChance = 0.1,
                    criticalMultiplier = 4.0,
                    minCriticalMultiplier = 2.0;

    private long maxHealth, health, power, evasion, accuracy, maxDamage, minDamage;
    private RPGCharacter character;
    protected Random random;
    private int stunDuration;

    public Fighter(RPGCharacter user) {
        this.character = user;
        maxHealth = user.getExperience();
        health = user.getExperience();
        power = user.getExperience();
        evasion = user.getExperience();
        accuracy = user.getExperience();
        random = new Random();
        calculateDamage();
    }

    private void calculateDamage() {
        maxDamage = (long)(0.3 * power);
        minDamage = (long)(0.2 * power);
        if(maxDamage < 1) maxDamage = 1;
        if(minDamage < 1) minDamage = 1;
    }

    public Strike strike(Fighter fighter) {

        long damage = minDamage + Math.round((maxDamage - minDamage) * random.nextDouble());

        Strike strike = new Strike();

        double hitRoll = random.nextDouble();
        if(hitRoll >= (1.0 - criticalMissChance)) {
            strike.setType(Strike.TYPE.CRITMISS);
        } else if(hitRoll < criticalHitChance) {
            strike.setType(Strike.TYPE.CRIT);
            double critModifier = criticalMultiplier * (1.0 + random.nextGaussian());
            if(critModifier < minCriticalMultiplier) critModifier = minCriticalMultiplier;
            damage = CommonUtil.multiply(damage,critModifier);
        } else {
            double dAcc = Math.log10(accuracy);
            double dEva = Math.log10(fighter.getEvasion());
            double hitChance = 0.75 * (1 + (dAcc - dEva) / 4);

            if(hitRoll < hitChance) {
                strike.setType(Strike.TYPE.HIT);
            } else {
                strike.setType(Strike.TYPE.MISS);
            }
        }

        strike.setDamage(damage);

        return strike;
    }

    public long getMaxHealth() {
        return maxHealth;
    }

    public long getHealth() {
        return health;
    }

    public long getPower() {
        return power;
    }

    public long getEvasion() {
        return evasion;
    }

    public long getAccuracy() {
        return accuracy;
    }

    public long getMaxDamage() {
        return maxDamage;
    }

    public void setMaxDamage(long maxDamage) {
        this.maxDamage = maxDamage;
    }

    public long getMinDamage() {
        return minDamage;
    }

    public void setMinDamage(long minDamage) {
        this.minDamage = minDamage;
    }

    public boolean damage(long damage) {
        health -= damage;
        return health <= 0;
    }

    public void levelUp() {
        double healthPercentage = (double)(health)/(double)(maxHealth);
        maxHealth = character.getExperience();
        health = (long)(maxHealth * healthPercentage);

        power = character.getExperience();
        evasion = character.getExperience();
        accuracy = character.getExperience();

        calculateDamage();
    }

    public void stun(int duration) {
        stunDuration = duration;
    }

    public void stunTick() {
        stunDuration--;
    }

    public boolean isStunned() {
        return stunDuration > 0;
    }
}
