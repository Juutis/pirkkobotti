package rpg.dungeon;

import model.Creature;
import model.RPGUser;

/**
 * Created by KM on 12.8.2015.
 */
public class Dungeon {

    private int level, depth;
    private Creature enemy;
    private CreatureGenerator generator;
    private RPGUser rpgUser;
    private boolean hc;

    private static final String START_ADVENTURE = "You enter a level DUNGEON_LEVEL dungeon.",
            DEFEATED = "You were defeated.",
            FIGHT = "A MONSTER_NAME is blocking your way. Do you wish to fight? [(y)es/(n)o].",
            LEAVE = "You leave the dungeon.";

    public Dungeon(int level, RPGUser user, boolean hc) {
        this.level = Math.min(10,Math.max(1,level));
        this.rpgUser = user;
        this.hc = hc;
        depth = 1;
        generator = CreatureGenerator.getSingleton();
        enemy = generator.getCreature(level, rpgUser, depth, hc);
    }

    public void goDeeper() {
        depth++;
        enemy = generator.getCreature(level, rpgUser, depth, hc);
    }

    public int getDepth() {
        return depth;
    }

    public Creature getEnemy() {
        if(enemy == null) {
            enemy = generator.getCreature(level, rpgUser, depth, hc);
        }
        return enemy;
    }

    public String getStartText() {
        return START_ADVENTURE.replace("DUNGEON_LEVEL",Integer.toString(level));
    }

    public String getFightText() {
        return FIGHT.replace("MONSTER_NAME",enemy.getName());
    }

    public String getDefeatedText() {
        return DEFEATED;
    }

    public String getLeaveText() {
        return LEAVE;
    }
}
