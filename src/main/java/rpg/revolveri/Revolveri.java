package rpg.revolveri;

import java.util.Random;

public class Revolveri
{
    private boolean[] rulla = new boolean[6];
    private int rullajoikkeli = 0;
    private int lkm = 0;
    Random rnd = new Random();

    public void pyorayta()
    {
        this.rullajoikkeli = this.rnd.nextInt(6);
    }

    public boolean vedaLiipasimesta()
    {
        this.rullajoikkeli += 1;
        if (this.rullajoikkeli > 5) {
            this.rullajoikkeli = 0;
        }
        if (this.rulla[this.rullajoikkeli])
        {
            this.rulla[this.rullajoikkeli] = false;
            this.lkm -= 1;
            return true;
        }
        return false;
    }

    public void lataa()
    {
        if (this.lkm < 6) {
            while(this.rulla[rullajoikkeli]) {
                this.rullajoikkeli++;
                if (this.rullajoikkeli > 5) {
                    this.rullajoikkeli = 0;
                }
            }
            this.rulla[rullajoikkeli] = true;
            lkm++;
        }
    }

    public int annaLkm()
    {
        return this.lkm;
    }
}
