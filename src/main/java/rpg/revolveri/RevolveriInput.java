package rpg.revolveri;

import main.Service;
import model.RPGUser;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

public class RevolveriInput implements Service
{
    private Revolveri revolveri = new Revolveri();
    private Channel channel;

    public RevolveriInput(Channel ch)
    {
        channel = ch;
    }

    public String handleMessage(RPGUser sender, PrivateMessageEvent event) {
        return null;
    }

    public String handleMessage(RPGUser sender, MessageEvent event)
    {
        String komento = event.getMessage();

        if (komento.equals("LATAA"))
        {
            return lataa();
        }
        else if (komento.equals("PYORAYTA"))
        {
            return pyorayta();
        }
        else if (komento.startsWith("AMMU"))
        {
            String[] tmp = komento.split(" ");
            if (tmp.length == 1) {
                return veda(sender.getNick());
            }
            if (tmp.length == 2) {
                return veda(tmp[1]);
            }
        }
        return null;
    }

    private String lataa()
    {
        this.revolveri.lataa();
        return "Ladataan... (" + this.revolveri.annaLkm() + "/6)";
    }

    private String pyorayta()
    {
        this.revolveri.pyorayta();
        return "PRRRRR...";
    }

    private String veda(String kayttaja)
    {
        if (this.revolveri.vedaLiipasimesta()) {
            channel.send().message(kayttaja + ": PAM!");
            for(org.pircbotx.User u : channel.getUsers()) {
                if(u.getNick().equals(kayttaja)) {
                    channel.send().kick(u, "Kuolit!");
                    break;
                }
            }
            return null;
        } else {
            return "KLIK!";
        }
    }
}
