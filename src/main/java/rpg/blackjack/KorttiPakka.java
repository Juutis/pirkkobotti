package rpg.blackjack;

import java.util.Random;

public class KorttiPakka
{
    private Kortti[] pakka;
    private Random r = new Random();
    private int joikkeli = 0;

    public KorttiPakka(int lkm)
    {
        this.pakka = new Kortti[52 * lkm];
        for (int j = 0; j < lkm; j++) {
            for (int i = 0; i < 13; i++)
            {
                Kortti k1 = new Kortti(i + 1, 0);
                Kortti k2 = new Kortti(i + 1, 1);
                Kortti k3 = new Kortti(i + 1, 3);
                Kortti k4 = new Kortti(i + 1, 2);

                this.pakka[(j * 52 + i)] = k1;
                this.pakka[(j * 52 + i + 13)] = k2;
                this.pakka[(j * 52 + i + 26)] = k3;
                this.pakka[(j * 52 + i + 39)] = k4;
            }
        }
        sekoita();
    }

    public void sekoita()
    {
        for (int i = 0; i < this.pakka.length; i++)
        {
            int rnd = this.r.nextInt(this.pakka.length);
            Kortti temp = this.pakka[rnd];
            this.pakka[rnd] = this.pakka[i];
            this.pakka[i] = temp;
        }
    }

    public Kortti nosta()
    {
        Kortti palautettava = this.pakka[this.joikkeli];
        this.joikkeli += 1;
        if (this.joikkeli >= this.pakka.length)
        {
            sekoita();
            this.joikkeli = 0;
        }
        return palautettava;
    }
}
