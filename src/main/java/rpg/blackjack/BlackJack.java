package rpg.blackjack;

import model.RPGUser;

import java.util.Vector;

public class BlackJack
{
    private Vector<Panos> panokset = new Vector();
    private RPGUser jakaja;
    private Panos omaPanos;
    private KorttiPakka pakka;

    public static final int PAKKA_LKM = 6;
    public static final int BLACKJACK = 21;
    public static final int OMARAJA = 16;

    public BlackJack(RPGUser p)
    {
        this.pakka = new KorttiPakka(6);
        this.jakaja = p;
        this.omaPanos = new Panos(this.jakaja, 0);
    }

    public void lisaaPanos(Panos p)
    {
        this.panokset.add(p);
    }

    public void jaa(Panos p)
    {
        p.lisaaKortti(this.pakka.nosta());
    }

    public void jaaKierros()
    {
        for (Panos p : this.panokset) {
            jaa(p);
        }
    }

    public void jaaItselle()
    {
        jaa(this.omaPanos);
    }

    public void jaaMukanaOlijoille()
    {
        for (Panos p : this.panokset) {
            if (p.lisaaKortteja()) {
                jaa(p);
            }
        }
    }

    public void jaaItselleLoput()
    {
        while (this.omaPanos.annaKorttienSumma() <= 16) {
            jaaItselle();
        }
    }

    public void poistaHavinneet()
    {
        for (Panos p : this.panokset) {
            if (p.annaKorttienSumma() > 21) {
                this.panokset.remove(p);
            }
        }
    }

    public boolean onkoPeliOhi()
    {
        for (Panos p : this.panokset) {
            if (p.lisaaKortteja()) {
                return false;
            }
        }
        return true;
    }

    public boolean onkoPanoksia()
    {
        return !this.panokset.isEmpty();
    }

    public Panos[] annaPanokset()
    {
        Panos[] panosTaulu = new Panos[this.panokset.size()];
        int i = 0;
        for (Panos p : this.panokset) {
            panosTaulu[(i++)] = p;
        }
        return panosTaulu;
    }

    public Panos annaJakajanPanos()
    {
        return this.omaPanos;
    }

    public void merkkaaIdlaajat()
    {
        for (Panos p : this.panokset) {
            if (!p.onkoValintaTehty())
            {
                p.valintaTehty(true);
                p.lisaaKortteja(false);
            }
        }
    }

    public void nollaaValinnat()
    {
        for (Panos p : this.panokset) {
            if (p.lisaaKortteja()) {
                p.valintaTehty(false);
            }
        }
    }

    public Panos etsiPanos(RPGUser pelaaja)
    {
        for (Panos p : this.panokset) {
            if (p.annaOmistaja().getNick().equals(pelaaja.getNick())) {
                return p;
            }
        }
        return null;
    }

    public void poistaPanokset()
    {
        this.panokset = new Vector();
        this.omaPanos = new Panos(this.jakaja, 0);
    }
}
