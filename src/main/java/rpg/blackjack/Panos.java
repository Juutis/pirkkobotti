package rpg.blackjack;

import model.RPGUser;

import java.util.Vector;

public class Panos
{
    private RPGUser omistaja;
    private long saldo;
    private Vector<Kortti> kortit = new Vector();
    private int assia = 0;
    private boolean lisaaKortteja = true;
    private boolean valintaTehty = false;

    public Panos(RPGUser o, long s)
    {
        this.omistaja = o;
        this.saldo = s;
    }

    public RPGUser annaOmistaja()
    {
        return this.omistaja;
    }

    public long annaSaldo()
    {
        return this.saldo;
    }

    public void lisaaKortti(Kortti k)
    {
        this.kortit.add(k);
        if (k.annaArvo() == 1) {
            this.assia += 1;
        }
        if (annaKorttienSumma() >= 21) {
            this.lisaaKortteja = false;
        }
    }

    public int annaKorttienSumma()
    {
        int summa = 0;
        for (Kortti k : this.kortit) {
            summa += k.annaBJarvo();
        }
        summa += this.assia * 10;
        int i = 0;
        while (this.assia > i)
        {
            if (summa <= 21) {
                break;
            }
            i++;
            summa -= 10;
        }
        return summa;
    }

    public boolean onkoBlackJack()
    {
        return (this.kortit.size() == 2) && (((((Kortti)this.kortit.get(1)).annaArvo() == 1) && (((Kortti)this.kortit.get(0)).annaArvo() == 10)) || ((((Kortti)this.kortit.get(0)).annaArvo() == 1) && (((Kortti)this.kortit.get(1)).annaArvo() == 10)));
    }

    public Kortti[] annaKortit()
    {
        Kortti[] korttiTaulu = new Kortti[this.kortit.size()];
        int i = 0;
        for (Kortti k : this.kortit)
        {
            korttiTaulu[i] = k;
            i++;
        }
        return korttiTaulu;
    }

    public boolean lisaaKortteja()
    {
        return this.lisaaKortteja;
    }

    public boolean onkoValintaTehty()
    {
        return this.valintaTehty;
    }

    public void lisaaKortteja(boolean b)
    {
        this.lisaaKortteja = b;
    }

    public void valintaTehty(boolean b)
    {
        this.valintaTehty = b;
    }

    public String toString()
    {
        String s = this.omistaja + ": ";
        for (Kortti k : this.kortit) {
            s = s + k;
        }
        int summa = annaKorttienSumma();
        if (onkoBlackJack()) {
            s = s + " = BLACKJACK";
        } else if (summa > 21) {
            s = s + " = YLI MENI";
        }
        return s;
    }
}
