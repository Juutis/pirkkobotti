package rpg.blackjack;

import model.RPGUser;

public class BlackJackInput
{
    private boolean otetaanKomentoja;
    private BlackJack blackjack;

    public BlackJackInput(BlackJack bj)
    {
        this.otetaanKomentoja = true;
        this.blackjack = bj;
    }

    public void kasittele(RPGUser lahettaja, String komento)
    {
        if (komento.equals("LISSEE"))
        {
            lisaaKortteja(lahettaja);
        }
        else if (komento.equals("EI ENNEE"))
        {
            lopeta(lahettaja);
        }
        else if (komento.startsWith("PANOSTA"))
        {
            String[] param = komento.split(" ");
            if (param.length == 2)
            {
                long maara = 0;
                if(param[1].equals("KAIKKI")) {
                    maara = lahettaja.getExperience();
                } else {
                    try
                    {
                        maara = Long.parseLong(param[1]);
                    }
                    catch (Exception e)
                    {
                        return;
                    }
                }
                panosta(lahettaja, maara);
            }
        }
    }

    private void panosta(RPGUser p, long maara)
    {
        if ((this.otetaanKomentoja) && (p.getExperience() >= maara) && (maara > 0) &&
                (this.blackjack.etsiPanos(p) == null))
        {
            Panos panos = new Panos(p, maara);
            this.blackjack.lisaaPanos(panos);
            this.blackjack.jaa(panos);
            this.blackjack.jaa(panos);
            panos.valintaTehty(true);
        }
    }

    private void lisaaKortteja(RPGUser p)
    {
        if (this.otetaanKomentoja)
        {
            Panos panos = this.blackjack.etsiPanos(p);
            if ((panos != null) && (!panos.onkoValintaTehty()) && (panos.lisaaKortteja()))
            {
                panos.valintaTehty(true);
                this.blackjack.jaa(panos);
            }
        }
    }

    private void lopeta(RPGUser p)
    {
        if (this.otetaanKomentoja)
        {
            Panos panos = this.blackjack.etsiPanos(p);
            if ((panos != null) && (!panos.onkoValintaTehty()))
            {
                panos.lisaaKortteja(false);
                panos.valintaTehty(true);
            }
        }
    }

    public void otetaanKomentoja(boolean b)
    {
        this.otetaanKomentoja = b;
    }
}
