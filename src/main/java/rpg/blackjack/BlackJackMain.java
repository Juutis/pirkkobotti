package rpg.blackjack;

import main.Service;
import model.RPGUser;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import util.UserUtil;

public class BlackJackMain extends Thread implements Service
{
    public static final String PREFIX = "!";
    public static final int ALKUSALDO = 1000;
    BlackJack blackjack;
    BlackJackInput blackjackinput;
    RPGUser poyta = null;
    boolean lopeta = false;

    private Channel channel;

    public BlackJackMain(Channel chan)
    {
        this.poyta = new RPGUser("Jakaja");
        this.blackjack = new BlackJack(this.poyta);
        this.blackjackinput = new BlackJackInput(this.blackjack);
        this.channel = chan;
        start();
    }

    public String handleMessage(RPGUser p, MessageEvent event)
    {
        this.blackjackinput.kasittele(p, event.getMessage());
        return null;
    }

    public void run()
    {
        while (!this.lopeta)
        {
            if (this.blackjack.onkoPanoksia()) {
                pelaaKierros();
            }
            try
            {
                sleep(1000L);
            }
            catch (Exception e)
            {
                return;
            }
        }
    }

    public void pelaaKierros()
    {
        channel.send().message("Blackjack-peli alkamassa! Panoksia otetaan 10s ajan.");
        try
        {
            sleep(10000L);
        }
        catch (Exception e)
        {
            return;
        }
        this.blackjackinput.otetaanKomentoja(false);
        this.blackjack.jaaItselle();
        this.blackjack.nollaaValinnat();
        while (!this.blackjack.onkoPeliOhi())
        {
            this.blackjackinput.otetaanKomentoja(false);
            channel.send().message(kortitStringiksi() + "   - 10s aikaa komennella!");
            this.blackjackinput.otetaanKomentoja(true);
            try
            {
                sleep(10000L);
            }
            catch (Exception e)
            {
                return;
            }
            this.blackjackinput.otetaanKomentoja(false);
            this.blackjack.merkkaaIdlaajat();
            this.blackjack.nollaaValinnat();
        }
        channel.send().message(kortitStringiksi());
        this.blackjack.jaaItselleLoput();
        channel.send().message(kortitStringiksi());
        channel.send().message(hoidaVoitot());

        this.blackjack.poistaPanokset();
        this.blackjackinput.otetaanKomentoja(true);
    }

    public String kortitStringiksi()
    {
        Panos[] panosTaulu = this.blackjack.annaPanokset();
        String tulos = this.blackjack.annaJakajanPanos().toString();
        for (int i = 0; i < panosTaulu.length; i++) {
            tulos = tulos + " | " + panosTaulu[i];
        }
        return tulos;
    }

    public String hoidaVoitot()
    {
        Panos[] panosTaulu = this.blackjack.annaPanokset();
        Panos jakaja = this.blackjack.annaJakajanPanos();
        int jakajansumma = jakaja.annaKorttienSumma();
        String tulos = "Voitot";
        for (int i = 0; i < panosTaulu.length; i++)
        {
            Panos tmp = panosTaulu[i];
            int tmpsumma = tmp.annaKorttienSumma();
            tulos = tulos + " | " + tmp.annaOmistaja() + ": ";
            boolean voititPelin = false;

            if (tmpsumma > 21)
            {
                tulos = tulos + "-" + tmp.annaSaldo();
                UserUtil.addXp(tmp.annaOmistaja(), -tmp.annaSaldo());
            }
            else if (jakaja.onkoBlackJack())
            {
                if (tmp.onkoBlackJack())
                {
                    tulos = tulos + "+0";
                }
                else
                {
                    tulos = tulos + "-" + tmp.annaSaldo();
                    UserUtil.addXp(tmp.annaOmistaja(), -tmp.annaSaldo());
                }
            }
            else if (tmp.onkoBlackJack())
            {
                long voitto = (long)(tmp.annaSaldo() * 1.5D);
                if(voitto < tmp.annaSaldo()) {
                    voitto = Long.MAX_VALUE;
                }

                tulos = tulos + "+" + voitto;
                voititPelin = UserUtil.addXp(tmp.annaOmistaja(), voitto);
            }
            else if (jakajansumma > 21)
            {
                tulos = tulos + "+" + tmp.annaSaldo();
                voititPelin = UserUtil.addXp(tmp.annaOmistaja(), tmp.annaSaldo());
            }
            else if (jakajansumma > tmpsumma)
            {
                tulos = tulos + "-" + tmp.annaSaldo();
                UserUtil.addXp(tmp.annaOmistaja(), -tmp.annaSaldo());
            }
            else if (jakajansumma < tmpsumma)
            {
                tulos = tulos + "+" + tmp.annaSaldo();
                voititPelin = UserUtil.addXp(tmp.annaOmistaja(), tmp.annaSaldo());
            }
            else if (jakajansumma == 21)
            {
                tulos = tulos + "+0";
            }
            else
            {
                tulos = tulos + "-" + tmp.annaSaldo();
                UserUtil.addXp(tmp.annaOmistaja(), -tmp.annaSaldo());
            }
            tulos = tulos + " (" + tmp.annaOmistaja().getExperienceString() + ")";

            if(voititPelin) {
                UserUtil.fireMaxLevelMessage(channel, tmp.annaOmistaja());
            }
        }
        return tulos;
    }

    public void lopeta()
    {
        this.lopeta = true;
    }

    public String handleMessage(RPGUser sender, PrivateMessageEvent event) {
        return null;
    }
}
