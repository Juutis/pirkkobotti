package rpg.blackjack;

public class Kortti
{
    private int arvo;
    private int vari;
    public static final int PATA = 0;
    public static final int HERTTA = 1;
    public static final int RUUTU = 2;
    public static final int RISTI = 3;
    public static final String[] VARIT = { "Pata", "Hertta", "Ruutu", "Risti" };

    public Kortti(int a, int v)
    {
        this.arvo = a;
        this.vari = v;
    }

    public int annaArvo()
    {
        return this.arvo;
    }

    public int annaBJarvo()
    {
        if (this.arvo > 10) {
            return 10;
        }
        return this.arvo;
    }

    public int annaVari()
    {
        return this.vari;
    }

    public String toString()
    {
        if (this.arvo == 1) {
            return "A";
        }
        if (this.arvo == 10) {
            return "T";
        }
        if (this.arvo == 11) {
            return "J";
        }
        if (this.arvo == 12) {
            return "Q";
        }
        if (this.arvo == 13) {
            return "K";
        }
        return Integer.toString(this.arvo);
    }
}