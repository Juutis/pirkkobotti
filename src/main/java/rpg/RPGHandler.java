package rpg;

import main.Service;
import main.UserManager;
import model.RPGUser;
import model.Trophy;
import org.pircbotx.Channel;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import util.CommonUtil;
import util.UserUtil;

import java.util.*;

/**
 * Created by KM on 6.8.2015.
 */
public class RPGHandler implements Service {

    public static final String YOU_WIN = "Saavutit kokemustason MAX_LEVEL! Voitit pelin!";
    public static final String YOU_WIN_REALLY = "Kukistit kaikki loppuvastukset! Voitit Suursankarien pelin!";
    public static final long YOU_WIN_DELAY = 5000;

    private static final String GIVE_XP = "!give xp",
                                LIST_XP = "!list xp",
                                MY_XP = "!my xp",
                                GIVE_SEX = "!give sex",
                                TOP_XP = "!top xp",
                                TOP_FRAGS = "!top frags",
                                TOP_TROPHY = "!top trophies";
    public static final int TOPLIST_LENGTH = 5;

    UserManager userManager;
    Random random;

    public RPGHandler(UserManager userManager) {
        this.userManager = userManager;
        random = new Random();
    }

    public String handleMessage(RPGUser sender, MessageEvent event) {
        String message = event.getMessage();

        if(message.equalsIgnoreCase(GIVE_XP)) {
            if (UserUtil.addXp(sender, 1L)) {
                UserUtil.fireMaxLevelMessage(event.getChannel(), sender);
            }
            return null;
        }
/*
        if(message.equalsIgnoreCase("!give max")) {
            UserUtil.addXp(sender, Long.MAX_VALUE);
            return null;
        }
*/

        if(message.equalsIgnoreCase(LIST_XP)) {
            return getXpList();
        }

        if(message.equalsIgnoreCase(TOP_XP)) {
            return getTopXpList();
        }

        if(message.equalsIgnoreCase(TOP_FRAGS)) {
            return getTopFraggers();
        }

        if(message.equalsIgnoreCase(TOP_TROPHY)) {
            printTopTrophies(event.getChannel());
        }

        if(message.equalsIgnoreCase(GIVE_SEX)) {
            if(!UserUtil.isMaxLevel(sender)) {
                return sender.getNick() + ": Ei kiitos. Ehkä jos kokemustasosi olisi MAX_LEVEL...";
            } else {
                return sender.getNick() + (random.nextInt(100) < 80 ? ": 8=====D~~~" : "( . Y . )");
            }
        }

        if(message.equals(MY_XP)) {
            return sender.getNick() + ": Nykyinen kokemustasosi on " + sender.getExperienceString() + ".";
        }

        return null;
    }

    public String handleMessage(RPGUser sender, PrivateMessageEvent event) {
        return null;
    }

    private String getTopXpList() {
        ArrayList<RPGUser> users = new ArrayList<RPGUser>(userManager.getUsers());
        Collections.sort(users, new Comparator<RPGUser>() {
            public int compare(RPGUser u1, RPGUser u2) {
                if(u1.getTopExperience() == u2.getTopExperience()) {
                    return 0;
                }
                if(u1.getTopExperience() < u2.getTopExperience()) {
                    return 1;
                }
                return -1;
            }
        });

        String topList = "Kaikkien aikojen himogrindaajat: ";
        boolean hasContent = false;

        for(int i = 0; i < TOPLIST_LENGTH && i < users.size(); i++) {
            RPGUser u = users.get(i);
            if(u.getTopExperience() > 0) {
                topList += i > 0 ? ", " : "";
                topList += (i+1) + ".) " + u.getNick() + ": " + u.getTopExperienceString() + " (" + CommonUtil.dateToString(u.getTopExperienceDate()) + ")";
                hasContent = true;
            }
        }

        if(!hasContent) {
            topList = "Kellään ei ole kokemustasoja.";
        }

        return topList;
    }

    private String getXpList() {
        ArrayList<RPGUser> users = new ArrayList<RPGUser>(userManager.getUsers());
        Collections.sort(users, new Comparator<RPGUser>() {
            public int compare(RPGUser u1, RPGUser u2) {
                if(u1.getExperience() == u2.getExperience()) {
                    return 0;
                }
                if(u1.getExperience() < u2.getExperience()) {
                    return 1;
                }
                return -1;
            }
        });

        String topList = "Tämän hetken himogrindaajat: ";
        boolean hasContent = false;

        for(int i = 0; i < TOPLIST_LENGTH && i < users.size(); i++) {
            RPGUser u = users.get(i);
            if(u.getExperience() > 0) {
                topList += i > 0 ? ", " : "";
                topList += (i+1) + ".) " + u.getNick() + ": " + u.getExperienceString();
                hasContent = true;
            }
        }

        if(!hasContent) {
            topList = "Kellään ei ole kokemustasoja.";
        }

        return topList;
    }

    private String getTopFraggers() {
        ArrayList<RPGUser> users = new ArrayList<RPGUser>(userManager.getUsers());
        Collections.sort(users, new Comparator<RPGUser>() {
            public int compare(RPGUser u1, RPGUser u2) {
                if(u1.getTrophies().size() == u2.getTrophies().size()) {
                    return 0;
                }
                if(u1.getTrophies().size() < u2.getTrophies().size()) {
                    return 1;
                }
                return -1;
            }
        });

        String topList = "Himoteurastajat: ";
        boolean hasContent = false;

        for(int i = 0; i < TOPLIST_LENGTH && i < users.size(); i++) {
            RPGUser u = users.get(i);
            if(u.getTrophies().size() > 0) {
                topList += i > 0 ? ", " : "";
                topList += (i+1) + ".) " + u.getNick() + ": " + u.getTrophies().size() + " päätä";
                hasContent = true;
            }
        }

        if(!hasContent) {
            topList = "Kellään ei ole kerättyjä päitä.";
        }

        return topList;
    }

    private void printTopTrophies(Channel channel) {
        ArrayList<RPGUser> users = new ArrayList<RPGUser>(userManager.getUsers());
        ArrayList<Trophy> trophies = new ArrayList<Trophy>(TOPLIST_LENGTH * users.size());

        for(RPGUser u : users) {
            trophies.addAll(u.getTrophies().getTopTrophies());
        }

        Collections.sort(trophies, new Comparator<Trophy>() {
            public int compare(Trophy t1, Trophy t2) {

                if(t1.getBossLevel() < t2.getBossLevel()) {
                    return 1;
                }
                if(t1.getBossLevel() > t2.getBossLevel()) {
                    return -1;
                }

                if (t1.getExperience() == t2.getExperience()) {
                    return 0;
                }
                if (t1.getExperience() < t2.getExperience()) {
                    return 1;
                }
                return -1;
            }
        });

        if(trophies.size() > 0) {
            channel.send().message("Voimakkaimmat päät: ");
        } else {
            channel.send().message("Kellään ei ole kerättyjä päitä.");
        }

        for(int i = 0; i < TOPLIST_LENGTH && i < trophies.size(); i++) {
            Trophy t = trophies.get(i);
            channel.send().message((i+1) + ".) " + t.getName() + ", " + t.getExperienceString() + " (" + t.getUser().getNick() + ", " + CommonUtil.dateToString(t.getDate()) + ")");
        }
    }

}
