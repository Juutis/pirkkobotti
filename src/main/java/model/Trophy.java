package model;

import util.UserUtil;

import java.util.Date;

/**
 * Created by KM on 14.8.2015.
 */
public class Trophy implements RPGCharacter {

    public static final String[] BOSS_LEVELS = {"",
                                                "SUPREME BOSS",
                                                "LEGENDARY BOSS",
                                                "ULTIMATE BOSS"};

    private String name;
    private long experience;
    private int id;
    private Date date;
    private RPGUser user;
    private int bossLevel;

    public Trophy(String name, long experience, Date date, RPGUser user, int bossLevel) {
        this(-1, name, experience, date, user, bossLevel);
    }

    public Trophy(int id, String name, long experience, Date date, RPGUser user, int bossLevel) {
        this.name = name;
        this.id = id;
        this.experience = experience;
        this.date = date;
        this.user = user;
        this.bossLevel = bossLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RPGUser getUser() {
        return user;
    }

    public void setUser(RPGUser user) {
        this.user = user;
    }

    public String getExperienceString() {
        if(bossLevel > 0) {
            return BOSS_LEVELS[bossLevel];
        }
        return UserUtil.isMaxLevel(this) ? "MAX_LEVEL" : Long.toString(getExperience()) + "xp";
    }

    public int getBossLevel() {
        return bossLevel;
    }

    public void setBossLevel(int bossLevel) {
        this.bossLevel = bossLevel;
    }
}
