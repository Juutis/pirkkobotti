package model;

/**
 * Created by KM on 13.8.2015.
 */
public class Strike {

    public static enum TYPE {HIT, MISS, CRIT, CRITMISS};
    private TYPE type;
    private long damage;

    public Strike() {
        this(0, TYPE.HIT);
    }

    public Strike(long damage, TYPE type) {
        this.damage = damage;
        this.type = type;
    }

    public long getDamage() {
        return damage;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public void setDamage(long damage) {
        this.damage = damage;
    }
}
