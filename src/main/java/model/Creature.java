package model;

import util.UserUtil;

/**
 * Created by KM on 11.8.2015.
 */
public class Creature implements RPGCharacter {

    private String name, shortName;
    private long xpBounty;
    private int bossLevel;

    public Creature(String name, String shortName, long xpBounty, int bossLevel) {
        this.name = name;
        this.shortName = shortName;
        this.xpBounty = xpBounty;
        this.bossLevel = bossLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getExperience() {
        return xpBounty;
    }
    public long getXpBounty() {
        return xpBounty;
    }

    public void setXpBounty(long xpBounty) {
        this.xpBounty = xpBounty;
    }

    public void addXpBounty(long xpBounty) {
        this.xpBounty += xpBounty;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String toString() {
        return name;
    }

    public String getExperienceString() {
        return UserUtil.isMaxLevel(this) ? "MAX_LEVEL" : Long.toString(getExperience()) + "xp";
    }

    public int getBossLevel() {
        return bossLevel;
    }

    public void setBossLevel(int bossLevel) {
        this.bossLevel = bossLevel;
    }
}
