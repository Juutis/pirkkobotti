package model;

import rpg.dungeon.Trophies;
import util.UserUtil;

import java.util.Date;

/**
 * Created by KM on 6.8.2015.
 */
public class RPGUser implements RPGCharacter {

    private String nick;
    private long experience, topExperience;
    private int id;
    private Date topExperienceDate;
    private Trophies trophies;

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public long getTopExperience() {
        return topExperience;
    }

    public void setTopExperience(long topExperience) {
        this.topExperience = topExperience;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTopExperienceDate() {
        return topExperienceDate;
    }

    public void setTopExperienceDate(Date topExperienceDate) {
        this.topExperienceDate = topExperienceDate;
    }

    public RPGUser(int id, String nick, long experience, long topExperience, Date topXpDate) {
        this.nick = nick;
        this.experience = experience;
        this.topExperience = topExperience;
        this.id = id;
        this.topExperienceDate = topXpDate;
        trophies = new Trophies(this);
    }

    public RPGUser(String nick) {
        this(-1, nick, 0, 0, new Date(0));
    }

    public String toString() {
        return getNick();
    }

    public String getExperienceString() {
        return UserUtil.isMaxLevel(this) ? "MAX_LEVEL" : Long.toString(getExperience()) + "xp";
    }

    public String getTopExperienceString() {
        return topExperience == Long.MAX_VALUE ? "MAX_LEVEL" : Long.toString(getTopExperience()) + "xp";
    }

    public Trophies getTrophies() {
        return trophies;
    }

    public void setTrophies(Trophies trophies) {
        this.trophies = trophies;
    }
}
