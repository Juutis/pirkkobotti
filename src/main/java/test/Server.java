package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by KM on 10.8.2015.
 */
public class Server {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(10101);
        Socket clientSocket = serverSocket.accept();
        System.out.println("Connection!");
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        while(true)
            System.out.println(reader.readLine());
    }
}
