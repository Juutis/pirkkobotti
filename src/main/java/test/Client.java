package test;

import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by KM on 10.8.2015.
 */
public class Client {

    public static void main(String[] args) throws Exception {
        long l = Long.MAX_VALUE + 1;
        System.out.println(Long.MAX_VALUE + " " + l);
        Socket socket = new Socket("localhost",10101);
        System.out.println("Connected from " + socket.getLocalPort());
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        while(true) {
            System.out.println("Sending.");
            out.println("plop!");
            Thread.sleep(1000);
        }
    }
}
