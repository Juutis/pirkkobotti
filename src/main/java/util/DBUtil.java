package util;

import model.RPGUser;
import model.Trophy;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by KM on 6.8.2015.
 */
public class DBUtil {

    private static final String USERS = "users";
    private static final String KEY_ID = "_id";
    private static final String KEY_NICK = "nick";
    private static final String KEY_SCORE = "score";
    private static final String KEY_TOPSCORE = "top_score";
    private static final String KEY_TOPSCOREDATE = "top_score_date";

    private static Connection connection;

    public static void initDB(String host, int port, String dbName, String userName, String password) throws Exception {
            if(connection == null) {
                connection = DriverManager.getConnection("jdbc:postgresql://" + host + ":" + port + "/" + dbName, userName, password);
                if(!tableExists(USERS)) {
                    createTables();
                }
            } else {
                throw new Exception("Database connection already initialized!");
            }
    }

    public static void saveUser(RPGUser user) {
        try {
            if (user.getId() == -1) {
                insertUser(user);
            } else {
                updateUser(user);
            }
        } catch (Exception e) {
            System.out.println("Failed to save user " + user.getNick() + ": " + e);
        }
    }

    public static void saveTrophy(Trophy trophy) {
        if(trophy.getId() == -1) {
            try {
                insertTrophy(trophy, trophy.getUser());
            } catch(Exception e) {
                System.out.println("Failed to save trophy " + trophy.getName() + " for user " + trophy.getUser().getNick() + ": " + e);
            }
        } else {
            throw new RuntimeException("Trying to update an existing trophy record. Is this intended?");
        }
    }

    public static Collection<RPGUser> getUsers() {
        ArrayList<RPGUser> users = new ArrayList<RPGUser>();
        try {
            String sql = "SELECT * FROM " + USERS;
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt(KEY_ID);
                String nick = rs.getString(KEY_NICK);
                long xp = rs.getLong(KEY_SCORE);
                long topXp = rs.getLong(KEY_TOPSCORE);
                Date topXpDate = rs.getDate(KEY_TOPSCOREDATE);
                RPGUser user = new RPGUser(id, nick, xp, topXp, new java.util.Date(topXpDate.getTime()));
                loadTrophies(user);
                users.add(user);
            }

        } catch (Exception e) {
            System.out.println("Failed to get users: " + e);
        }
        return users;
    }

    public static void loadTrophies(RPGUser user) {
        try {
            String sql = "SELECT * FROM trophies WHERE user_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1,user.getId());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("_id");
                String name = rs.getString("name");
                long xp = rs.getLong("score");
                Date date = rs.getDate("date");
                int bossLevel = rs.getInt("bosslevel");
                user.getTrophies().addTrophy(new Trophy(id, name, xp, date, user, bossLevel));
            }

        } catch (Exception e) {
            System.out.println("Failed to get trophies for user " + user.getId() + ": " + e);
        }
    }

    private static void insertUser(RPGUser user) throws Exception {
        String sql = "INSERT INTO " + USERS + " (" + KEY_NICK + "," + KEY_SCORE + "," + KEY_TOPSCORE + "," + KEY_TOPSCOREDATE + ") VALUES (?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, user.getNick());
        ps.setLong(2, user.getExperience());
        ps.setLong(3, user.getTopExperience());
        ps.setDate(4, new Date(user.getTopExperienceDate().getTime()));

        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()) {
           user.setId(rs.getInt(1));
        }
    }

    private static void updateUser(RPGUser user) throws Exception {
        String sql = "UPDATE " + USERS + " SET " + KEY_NICK + " = ?, " + KEY_SCORE + " = ?, " + KEY_TOPSCORE + " = ?, " + KEY_TOPSCOREDATE + " = ? WHERE " + KEY_ID + " = ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, user.getNick());
        ps.setLong(2, user.getExperience());
        ps.setLong(3, user.getTopExperience());
        ps.setDate(4, new Date(user.getTopExperienceDate().getTime()));
        ps.setInt(5, user.getId());

        ps.executeUpdate();
    }

    private static void insertTrophy(Trophy trophy, RPGUser user) throws Exception {
        String sql = "INSERT INTO trophies (user_id, name, score, date, bosslevel) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setInt(1, user.getId());
        ps.setString(2, trophy.getName());
        ps.setLong(3, trophy.getExperience());
        ps.setDate(4, new Date(trophy.getDate().getTime()));
        ps.setInt(5, trophy.getBossLevel());

        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()) {
            trophy.setId(rs.getInt(1));
        }
    }

    private static void createTables() throws Exception {
        String sql = "CREATE TABLE " + USERS + " (" +
                KEY_ID + " SERIAL PRIMARY KEY" +
                "," + KEY_NICK + " VARCHAR(50)" +
                "," + KEY_SCORE + " BIGINT" +
                "," + KEY_TOPSCORE + " BIGINT" +
                "," + KEY_TOPSCOREDATE + " DATE)";

        PreparedStatement ps = connection.prepareStatement(sql);

        ps.execute();
    }

    private static boolean tableExists(String tableName) throws Exception {
        String sql = "SELECT 1 FROM information_schema.tables " +
                "WHERE table_schema = ? AND table_name = ?;";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "public");
        ps.setString(2, tableName);
        ResultSet result = ps.executeQuery();
        if(result.next()) {
            return true;
        }
        return false;
    }

}
