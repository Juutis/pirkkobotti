package util;

import model.RPGCharacter;
import model.RPGUser;
import org.pircbotx.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rpg.RPGHandler;

import java.util.Date;

/**
 * Created by KM on 6.8.2015.
 */
public class UserUtil {

    private static final Logger LOG = LoggerFactory.getLogger(UserUtil.class);

    public static boolean addXp(RPGUser user, long amount) {

        if (amount < Long.MAX_VALUE - user.getExperience()) {
            return setXp(user, user.getExperience() + amount);
        } else {
            return setXp(user, Long.MAX_VALUE);
        }
    }

    public static boolean setXp(RPGUser user, long amount) {

        boolean reachedMax = false;

        if(amount == Long.MAX_VALUE && user.getExperience() < Long.MAX_VALUE) {
            reachedMax = true;
        }
        user.setExperience(amount);

        if(user.getExperience() > user.getTopExperience()) {
            user.setTopExperience(user.getExperience());
            user.setTopExperienceDate(new Date());
        }

        DBUtil.saveUser(user);

        return reachedMax;
    }

    public static boolean isMaxLevel(RPGCharacter user) {
        return user.getExperience() == Long.MAX_VALUE;
    }

    public static void fireMaxLevelMessage(Channel channel, RPGUser user) {
        new MaxLevelMessageSender(channel, user, RPGHandler.YOU_WIN, RPGHandler.YOU_WIN_DELAY);
    }

    public static void fireReallyWonMessage(Channel channel, RPGUser user) {
        new MaxLevelMessageSender(channel, user, RPGHandler.YOU_WIN_REALLY, RPGHandler.YOU_WIN_DELAY);
    }
}

class MaxLevelMessageSender extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(MaxLevelMessageSender.class);

    private Channel channel;
    private RPGUser user;
    private String message;
    private long delay;

    public MaxLevelMessageSender(Channel channel, RPGUser user, String message, long delay) {
        this.channel = channel;
        this.user = user;
        this.message = message;
        this.delay = delay;
        this.start();
    }

    public void run() {
        try {
            sleep(delay);
        } catch (Exception e) {
            LOG.warn("Sleep interrupted!", e);
        }
        channel.send().message(user.getNick() + ": " + message);
    }
}
