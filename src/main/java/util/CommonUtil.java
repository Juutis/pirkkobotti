package util;

import java.util.*;

/**
 * Created by KM on 12.8.2015.
 */
public class CommonUtil {

    private static Random random = new Random();

    public static long add(long long1, long long2) {
        if(long1 > 0 && long2 > 0) {
            if (long1 > Long.MAX_VALUE - long2) {
                return Long.MAX_VALUE;
            }
        }
        if(long1 < 0 && long2 < 0) {
            if (long1 < Long.MIN_VALUE - long2) {
                return Long.MIN_VALUE;
            }
        }
        return long1 + long2;
    }

    public static long multiply(long long1, long long2) {
        if(long1 > Math.floor(Long.MAX_VALUE / long2)) {
            return Long.MAX_VALUE;
        }
        return long1 * long2;
    }

    public static long multiply(long long1, double double2) {
        if(long1 > Math.floor(Long.MAX_VALUE / double2)) {
            return Long.MAX_VALUE;
        }
        return (long)(long1 * double2);
    }

    public static void shuffle(String[] array) {
        for(int i = 0; i < array.length; i++) {
            int rnd = random.nextInt(array.length);
            String tmp = array[i];
            array[i] = array[rnd];
            array[rnd] = tmp;
        }
    }

    public static boolean hasContent(String s) {
        return s != null && !s.isEmpty();
    }

    public static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String dateToString(Date date) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        return c.get(Calendar.DATE) + "." + (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
    }

    public static boolean containsSubstring(Collection<String> strings, String substring) {
        for(String s : strings) {
            if(s.indexOf(substring) > -1) return true;
        }
        return false;
    }
}
